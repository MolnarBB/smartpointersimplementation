#pragma once
#include <iostream>
template <typename T>
class GenericSharedPtr
{
public:
	static constexpr int kDefaultReferenceCount = 1;
public:
	GenericSharedPtr() = default;
	GenericSharedPtr(T* ptr);

	GenericSharedPtr(const GenericSharedPtr& other);
	GenericSharedPtr& operator =(const GenericSharedPtr& other);

	GenericSharedPtr(GenericSharedPtr&& other);
	GenericSharedPtr& operator=(GenericSharedPtr&& other);

	T& operator*() { return *_ptr; }

	const T* operator->() const { return _ptr; }
	T* operator->() { return _ptr; }

	~GenericSharedPtr()
	{
		*_referenceCount -= 1;

		if (*_referenceCount == 0)
		{
			std::cout << "Am murit.\n";
			delete _ptr;
		}
	}

	int use_count() const {
		return *_referenceCount;
	}

private:
	int* _referenceCount;
	T* _ptr;	
};

template<typename T>
inline GenericSharedPtr<T>::GenericSharedPtr(T * ptr)
	: _ptr(ptr),
	_referenceCount(new int())
{
	*_referenceCount = 1;
}

template<typename T>
inline GenericSharedPtr<T>::GenericSharedPtr(const GenericSharedPtr & other)
{
	_referenceCount = other._referenceCount;
	*_referenceCount+=1;
	_ptr = other._ptr;
}

template <typename T>
GenericSharedPtr<T>& GenericSharedPtr<T>::operator=(const GenericSharedPtr<T>& other)
{
	if (this == &other)
		return *this;

	_referenceCount = other._referenceCount;
	_ptr = other._ptr;
	*_referenceCount += 1;

	return *this;
}

template<typename T>
inline GenericSharedPtr<T>::GenericSharedPtr(GenericSharedPtr && other)
{
	_referenceCount = other._referenceCount;
	_ptr = other._ptr;
	other._ptr = nullptr;
	*(other._referenceCount) = 0;
}

template <typename T>
GenericSharedPtr<T>& GenericSharedPtr<T>::operator=(GenericSharedPtr&& other)
{
	_referenceCount = other._referenceCount;
	_ptr = other._ptr;
	other._ptr = nullptr;
	other._referenceCount = 0;
}
