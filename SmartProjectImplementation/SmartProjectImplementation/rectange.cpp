#include "rectange.h"

#include <iostream>

Rectangle::Rectangle(std::string name, double width, double height):
	Shape(name),
	m_width(width), 
	m_height(height)
{}



Rectangle::~Rectangle()
{

	std::cout << "Rectangle: " << m_name << " was deleted.\n";
}

double Rectangle::getArea() const
{
	return m_width * m_height;
}
