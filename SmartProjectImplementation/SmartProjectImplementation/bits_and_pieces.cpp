#pragma once

#include "bits_and_pieces.h"
#include <limits>

void Animal::getSomeLove(unsigned int loveAmount) { m_happiness += loveAmount; }

unsigned int Heart::lovePotential()const
{
    switch (m_size)
    {
    case Size::Small : return 1;
    case Size::Medium : return 10;
    case Size::Large : return 100;
    case Size::Infinite : return std::numeric_limits<unsigned int>::infinity();
    default: return 0;
    }
}
void Heart::love(GenericSharedPtr<Animal> a)
{
    a->getSomeLove(lovePotential());
}

std::ostream& operator<<(std::ostream& stream, Idea const & idea)
{
    return stream << idea.content();
}

std::ostream& operator<<(std::ostream& stream, Brain const& brain)
{
    for (auto idea : brain.knowledge())
    {
        stream << *idea << std::endl;
    }
    return stream;
}


