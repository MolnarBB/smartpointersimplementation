#pragma once
#include "Shape.h"
class Circle: public Shape
{
public:
	Circle(std::string name, double raze);
	~Circle();
	double getArea() const;

private:
	double m_radius;
};

