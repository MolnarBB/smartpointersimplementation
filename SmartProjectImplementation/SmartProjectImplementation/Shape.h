#pragma once
#include <string>

class Shape
{
public:
	Shape(std::string name);
	const std::string& getName() const;
	virtual double getArea() const = 0;
	virtual ~Shape();
protected:
	const std::string m_name;

};

