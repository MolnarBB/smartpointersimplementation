#include "Circle.h"
#include "Shape.h"

#include <iostream>

#define pi 3.14

Circle::Circle(std::string name, double radius):Shape(name), m_radius(radius)
{}


Circle::~Circle()
{
	std::cout << "Circle: " << m_name << " was destroyed.\n";
}

double Circle::getArea() const
{
	return pi * m_radius * m_radius;
}
