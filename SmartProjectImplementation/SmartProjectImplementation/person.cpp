#pragma once

#include "person.h"
#include "bits_and_pieces.h"

Person::Person(std::string const & name)
    : m_name{ name }
    , m_brain{ new Brain() }
    , m_heart{ new Heart(Heart::Size::Medium) }
{

}

Person::~Person()
{

}


void Person::love(GenericSharedPtr<Animal> a)
{
    m_heart->love(a);
}

void Person::learn(GenericSharedPtr<Idea> idea)
{
    m_brain->store(idea);
}
GenericSharedPtr<Idea> Person::understand(GenericSharedPtr<Idea> idea)
{
   // return new Idea(*idea);
	return idea;
}


void Person::teach(GenericSharedPtr<Person> p, GenericSharedPtr<Idea> idea)
{
    p->learn(understand(idea));
}

std::vector<GenericSharedPtr<Idea>> Person::knowledge() const
{
    return m_brain->knowledge();
}


