#pragma once
#include "Shape.h"
#include <iostream>

template <typename T>
class GenericUniquePtr
{
public:
	GenericUniquePtr(T* shape);
	GenericUniquePtr(const GenericUniquePtr& other) = delete;                       //copy const
	GenericUniquePtr& operator = (const GenericUniquePtr& other) = delete;       //copy asigment operator
	GenericUniquePtr& operator = (GenericUniquePtr&& other);        //move asigment operator
	GenericUniquePtr(GenericUniquePtr&& other);                //move consturctor
	~GenericUniquePtr();
	const T& operator*() const;
	T& operator*();
	const T* operator->() const { return m_ptr; }
	T* operator->() { return m_ptr; }
private:
	T* m_ptr;
};

template<typename T>
GenericUniquePtr<T>::GenericUniquePtr(T* shape) : m_ptr(shape)
{}

template<typename T>
GenericUniquePtr<T>& GenericUniquePtr<T>::operator = (GenericUniquePtr<T>&& other)
{
	if (this == &other)
		return *this;

	m_ptr = other.m_ptr;
	other.m_ptr = nullptr;
	return *this;
}


template<typename T>
GenericUniquePtr<T>::GenericUniquePtr(GenericUniquePtr<T> && other)
{
	m_ptr = other.m_ptr;
	other.m_ptr = nullptr;
}

template<typename T>
GenericUniquePtr<T>::~GenericUniquePtr()
{
	delete m_ptr;
}

template<typename T>
const T& GenericUniquePtr<T>::operator*() const
{
	return *m_ptr;
}
template<typename T>
T& GenericUniquePtr<T>::operator*()
{
	return *m_ptr;
}



