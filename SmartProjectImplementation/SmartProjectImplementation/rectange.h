#pragma once
#include "Shape.h"
class Rectangle: public Shape
{
public:
	Rectangle(std::string name, double width, double height);
	~Rectangle();
	double getArea() const;
private:
	double m_width;
	double m_height;
};

