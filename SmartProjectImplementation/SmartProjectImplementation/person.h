#pragma once

#include "ShapePointer.h"
#include "GenericSharedPtr.hpp"
#include <string>
#include <vector>

class Brain;
class Heart;
class Animal;
class Idea;

class Person
{
public:
    Person(std::string const & name);
    ~Person();

    std::string getName() const { return m_name; }
 
    void love(GenericSharedPtr<Animal> a);
    void learn(GenericSharedPtr<Idea> idea);
	GenericSharedPtr<Idea> understand(GenericSharedPtr<Idea> idea);
    void teach(GenericSharedPtr<Person> p, GenericSharedPtr<Idea> idea);
    std::vector<GenericSharedPtr<Idea>> knowledge() const;  //shard poiter

	//void meet(Person*p)
//	{

	//}

private:
    std::string m_name;
	GenericUniquePtr<Brain> m_brain;
	GenericUniquePtr<Heart> m_heart;
};
