#pragma once
#include "Shape.h"
class Square : public Shape
{
public:
	Square(std::string name, double dimension);
	double getArea() const override;
	~Square();
private:
	double m_dimension;
	int * x{ new int };
};

