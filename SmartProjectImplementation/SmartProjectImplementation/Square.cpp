#include "Square.h"

#include <iostream>

Square::Square(std::string name, double dimension) :Shape(name), m_dimension(dimension)
{}


double Square::getArea() const
{
	return m_dimension* m_dimension;
}

Square::~Square()
{
	std::cout << "Square: " << m_name << " was deleted.\n";
	delete x;
}
