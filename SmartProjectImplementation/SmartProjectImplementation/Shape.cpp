#include "Shape.h"


#include <iostream>

Shape::Shape(std::string name):m_name(name)
{}

const std::string & Shape::getName() const
{
return m_name;
}

Shape::~Shape()
{
	std::cout << "Shape: " <<  m_name << " was destroyed.\n";
}
