#include <iostream>
#include <fstream>
#include <vector>
#include <memory>
#include <unordered_map>
#include <assert.h>

#include "ShapePointer.h"
#include "GenericSharedPtr.hpp"
#include "rectange.h"
#include "Square.h"
#include "Circle.h"



 #include <sstream>

#include "person.h"
#include "bits_and_pieces.h"

Person * spawnPerson(std::string const & name)
{
	return new Person(name);
}

std::string generatePersonName()
{
	static unsigned int personNumber = 0;
	std::ostringstream sstream;
	sstream << "Person_" << personNumber++;
	return sstream.str();
}

void startSimulator()
{
	bool simulatorRunning = true;

	GenericSharedPtr<Animal> theAlmightyCat = new Animal("cat");
	GenericSharedPtr<Idea> idea = new Idea("The earth is flat");
	while (simulatorRunning)
	{
		Person * p = spawnPerson(generatePersonName());
		p->learn(idea);
		p->love(theAlmightyCat);
		for ( auto idea : p->knowledge())
		{
			std::cout << p->getName() << " knows that: " << *idea << std::endl;
		}
		

		simulatorRunning = false;
	}
}

/*
class DanceClub
{
public:
	void addMember(GenericSharedPtr<Person> p) { persons.push_back(p); }

private:
	std::vector<GenericSharedPtr<Person>> persons;
};

*/
int main()
{
	/* 
	GenericSharedPtr<Person> pers1(new Person("pers1"));
	GenericSharedPtr<Person> pers2(new Person("pers2"));
	
	DanceClub tangoClub;
	tangoClub.addMember(pers1);

	DanceClub salsaClub;
	salsaClub.addMember(pers2);
	salsaClub.addMember(pers2);
	*/

	
	startSimulator();

	std::ifstream file("in.txt");
	std::string currentLine;
	std::vector<std::unique_ptr<Shape>> Shapes;
	std::vector<GenericUniquePtr<Shape>> ShapePtr;

	if (!file.is_open())
	{
		std::cout << "Could not open your current txt file!\n";
		exit(-1);
	}

	while (std::getline(file, currentLine))
	{
		std::string name;
		if (currentLine == "square")
		{	
			std::string dimensionString;

			std::getline(file, name);
			std::getline(file, dimensionString);

			auto square = std::make_unique<Square>(name, std::stoi(dimensionString));
			Shapes.push_back(std::move(square));
			//
			
			GenericUniquePtr<Shape> sp(new Square(name, std::stoi(dimensionString)));
			
			
			ShapePtr.push_back(std::move(sp));
			
			
		}

		if (currentLine == "circle")
		{
			std::string radius;

			std::getline(file, name);
			std::getline(file, radius);

			auto circle = std::make_unique<Circle>(name, std::stod(radius));
			Shapes.push_back(std::move(circle));

			GenericUniquePtr<Shape> shapePointer(new Circle(name, std::stod(radius)));
			
			ShapePtr.push_back(std::move(shapePointer));

		}


		if (currentLine == "rectangle")
		{

			std::string dimensionsRectangle;

			std::getline(file, name);
			std::getline(file, dimensionsRectangle);
			double width = std::stod(dimensionsRectangle);
			std::getline(file, dimensionsRectangle);
			double height = std::stod(dimensionsRectangle);
			
			auto rectangle = std::make_unique<Rectangle>(name, width, height);
			Shapes.push_back(std::move(rectangle));

			GenericUniquePtr<Shape> shapePointer(new Rectangle(name, width, height));
			ShapePtr.push_back(std::move(shapePointer));
		}

	}

	//for (auto& shape : Shapes)
	//	std::cout << shape->getName() << "\n";
	//	
	//std::cout << "Using our shared pointer" << "\n";

	for (auto& shape : ShapePtr)
	{
		std::cout << shape->getName() << "\n";
	}
	GenericSharedPtr<int> p1 = new int;
	
	*p1 = 5;
	auto p2 = p1;
	auto p4 = p1;
	assert(p4.use_count() == p1.use_count());
	*p2 = 9;
	{
		auto p10 = p4;
	}
	GenericSharedPtr<int> p5;
	p5 = p4;
	std::cout << *p1 << std::endl;

	std::cout << "p1 use count: " << p1.use_count() << std::endl;
	std::cout << "p2 use count: " << p2.use_count() << std::endl;
	std::cout << "p1 use count: " << p1.use_count() << std::endl;

	return 0;
}