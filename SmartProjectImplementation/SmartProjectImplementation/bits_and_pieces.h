#pragma once

#include <string>
#include <vector>
#include "GenericSharedPtr.hpp"

class Idea {
public:
    Idea(const Idea& other) : m_content { other.m_content } { }
    explicit Idea(std::string const & content) : m_content{ content } { }
    std::string content() const { return m_content; }

    friend std::ostream& operator<<(std::ostream& os, Idea const& idea);

private:
    std::string m_content;
};

class Heart;
class Animal {
public:
    Animal(std::string const & name) : m_name{ name } { }

    void getSomeLove(unsigned int loveAmount);
    int getHappiness() const { return m_happiness; }

private:
    std::string m_name;
    int m_happiness{ 0 };
};

class Brain {
public:
    void store(GenericSharedPtr<Idea> idea)
    {
        m_knowledge.push_back(idea);
    }

    std::vector<GenericSharedPtr<Idea>> knowledge() const { return m_knowledge; }

    friend std::ostream& operator<<(std::ostream& stream, Brain const& brain);

private:
    std::vector<GenericSharedPtr<Idea>> m_knowledge;
};

class Heart {
public:
    enum Size {
        Small,
        Medium,
        Large,
        Infinite
    };
    Heart(Size const& size) : m_size{ size } { }

    Size size() const { return m_size; }
    void love(GenericSharedPtr<Animal> a);

private:
    unsigned int lovePotential() const;

private:
    Size m_size;
};
